# Gerrit Rebuild Plugin

![Screenshot](https://gitlab.com/kyellow/gerrit-rebuild-plugin/raw/master/rebuild_plugin.png)

## Description
This gerrit plugin has been developed to work with buildbot to allow event raises from the web interface of the patch-sets which can not complete the build steps properly.

## How to build
The fact that plugin contains BUILD file doesn’t mean that building this plugin from the plugin directory works.
Bazel in tree driven means it can only be built from within Gerrit tree. Clone or link the plugin into gerrit/plugins directory:

  cd gerrit
  bazel build plugins/rebuild:rebuild

The output can be normally found in the following directory:

  bazel-genfiles/plugins/rebuild/rebuild.jar
