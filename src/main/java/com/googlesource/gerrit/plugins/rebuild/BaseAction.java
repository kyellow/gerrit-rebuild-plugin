// Copyright (C) 2013 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.rebuild;

import com.google.common.base.Strings;
import com.google.gerrit.common.TimeUtil;
import com.google.gerrit.extensions.restapi.RestApiException;
import com.google.gerrit.reviewdb.client.Change;
import com.google.gerrit.reviewdb.client.Change.Status;
import com.google.gerrit.reviewdb.client.ChangeMessage;
import com.google.gerrit.reviewdb.client.PatchSet;
import com.google.gerrit.reviewdb.server.ReviewDb;
import com.google.gerrit.server.ChangeUtil;
import com.google.gerrit.server.CurrentUser;
import com.google.gerrit.server.IdentifiedUser;
import com.google.gerrit.server.git.BatchUpdate;
import com.google.gerrit.server.git.UpdateException;
import com.google.gerrit.server.index.change.ChangeIndexer;
import com.google.gerrit.server.notedb.ChangeUpdate;
import com.google.gwtorm.server.OrmException;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.IOException;
import java.util.Collections;
import java.util.UUID;
import java.util.ArrayList;

import com.google.gerrit.server.events.PatchSetCreatedEvent;
import com.google.gerrit.common.EventDispatcher;
import com.google.gerrit.extensions.registration.DynamicItem;
import com.google.gerrit.server.events.EventFactory;
import com.google.gerrit.server.project.ProjectCache;
import com.google.gerrit.server.git.GitRepositoryManager;
import com.google.gerrit.server.PatchSetUtil;
import org.eclipse.jgit.revwalk.RevWalk;
import com.google.gerrit.reviewdb.client.Account;
import org.eclipse.jgit.lib.Repository;
import com.google.gerrit.server.change.RevisionResource;
import com.google.gerrit.server.data.ChangeAttribute;
import com.google.gerrit.server.data.PatchSetAttribute;
import com.google.gerrit.server.data.AccountAttribute;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.gerrit.extensions.common.AccountInfo;
import com.google.gerrit.reviewdb.client.PatchSetApproval;

abstract class BaseAction {
	static class Input {
		String message;
	}

	private final Provider<ReviewDb> dbProvider;
	private final Provider<CurrentUser> userProvider;
	private final ChangeIndexer indexer;
	private final BatchUpdate.Factory batchUpdateFactory;

	private final EventFactory eventFactory;
	private final DynamicItem<EventDispatcher> eventDispatcher;
	private final ProjectCache projectCache;
	private final GitRepositoryManager repoManager;
	private final PatchSetUtil psUtil;

	@Inject
	BaseAction(Provider<ReviewDb> dbProvider,
			Provider<CurrentUser> userProvider,
			EventFactory eventFactory,
			ProjectCache projectCache,
			GitRepositoryManager repoManager,
			PatchSetUtil psUtil,
			ChangeIndexer indexer,
			DynamicItem<EventDispatcher> eventDispatcher,
			BatchUpdate.Factory batchUpdateFactory) {
		this.dbProvider = dbProvider;
		this.userProvider = userProvider;
		this.indexer = indexer;
		this.batchUpdateFactory = batchUpdateFactory;

		this.eventDispatcher = eventDispatcher;
		this.eventFactory = eventFactory;
		this.projectCache = projectCache;
		this.repoManager = repoManager;
		this.psUtil = psUtil;
	}

	protected void changeStatus(Change change, final PatchSet.Id psId,
			Input input, final Status from, final Status to, RevisionResource rrsc)
		throws OrmException, RestApiException, IOException, UpdateException {

		ReviewDb db = dbProvider.get();
		Change.Id changeId = change.getId();
		db.changes().beginTransaction(changeId);
		try (BatchUpdate bu = batchUpdateFactory.create(
					db, change.getProject(), userProvider.get(), TimeUtil.nowTs())) {
			bu.addOp(change.getId(), new BatchUpdate.Op() {
				@Override
				public boolean updateChange(BatchUpdate.ChangeContext ctx) {
					Change change = ctx.getChange();
					ChangeUpdate update = ctx.getUpdate(psId);
					if (change.getStatus() == from) {
						change.setStatus(to);
						update.setStatus(change.getStatus());
						return true;
					}
					return false;
				}
			});
			bu.execute();

			ArrayList<PatchSetApproval> alist = new ArrayList<PatchSetApproval>();

			for( PatchSetApproval a : db.patchSetApprovals().byChange(changeId))
			{
				if( a.getLabel().equals("Code-Review"))
					continue;
				else
					alist.add(a);
			}

			db.patchSetApprovals().delete(alist);
			db.changeMessages().insert(Collections.singleton(
						newMessage(input, change)));

			db.commit();
		} finally {
			db.rollback();
		}

		indexer.index(db, change);

		try
		{
			PatchSetCreatedEvent event2 = new PatchSetCreatedEvent(rrsc.getChange());

			event2.change = changeAttributeSupplier(rrsc.getChange());
			event2.patchSet = patchSetAttributeSupplier(rrsc.getChange(), rrsc.getPatchSet());
			event2.uploader = accountAttributeSupplier(userProvider.get().getAccountId());

			eventDispatcher.get().postEvent(rrsc.getChange(),event2); 
		}
		catch(Exception e)
		{
			//
		}
	}


	private Supplier<ChangeAttribute> changeAttributeSupplier(
			final Change change) {
		return Suppliers.memoize(
				new Supplier<ChangeAttribute>() {
					@Override
					public ChangeAttribute get() {
						return eventFactory.asChangeAttribute(change);
					}
				});
			}
	private Supplier<AccountAttribute> accountAttributeSupplier(
			final Account.Id account) {
		return Suppliers.memoize(
				new Supplier<AccountAttribute>() {
					@Override
					public AccountAttribute get() {
						return account != null ? eventFactory.asAccountAttribute(account) : null;
					}
				});
			}

	private Supplier<PatchSetAttribute> patchSetAttributeSupplier(
			final Change change, final PatchSet patchSet) {
		return Suppliers.memoize(
				new Supplier<PatchSetAttribute>() {
					@Override
					public PatchSetAttribute get() {
						try (Repository repo =
								repoManager.openRepository(change.getProject());
								RevWalk revWalk = new RevWalk(repo)) {
							return eventFactory.asPatchSetAttribute(
									revWalk, change, patchSet);
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
				});
			}

	private ChangeMessage newMessage(Input input,
			Change change) throws OrmException {

		StringBuilder buf = new StringBuilder(" Rebuild : ");
		String msg = Strings.nullToEmpty(input.message).trim();

		if (!msg.isEmpty()) {
			buf.append("\n\n");
			buf.append(msg);
		}

		ChangeMessage message = new ChangeMessage(
				new ChangeMessage.Key(
					change.getId(),
					UUID.randomUUID().toString().replaceAll("-", "")),
				((IdentifiedUser)userProvider.get()).getAccountId(),
				change.getLastUpdatedOn(),
				change.currentPatchSetId());
		message.setMessage(msg.toString());
		return message;
	}

	protected static String status(Change change) {
		return change != null
			? change.getStatus().name().toLowerCase()
			: "deleted";
	}
}
